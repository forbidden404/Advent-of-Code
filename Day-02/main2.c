#include <stdio.h>

#define max(a,b) ((a) > (b) ? (a) : (b))

int main()
{
	freopen("input", "r", stdin);
	int l, w, h;
	int square_feet = 0;

	while (scanf("%d%*c%d%*c%d", &l, &w, &h) != EOF) {
		square_feet += (l * w * h);
		if (max(max(l,w),h) == l) 
			square_feet += (2*w + 2*h);
		else if (max(max(l,w),h) == w)	
			square_feet += (2*l + 2*h);
		else
			square_feet += (2*l + 2*w);
	}

	printf("%d\n", square_feet);
	return 0;
}
