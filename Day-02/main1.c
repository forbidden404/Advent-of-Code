#include <stdio.h>

#define min(a,b) ((a) > (b) ? (b) : (a))

int main()
{
	int l, w, h;
	int size_a, size_b, size_c;
	int square_feet = 0;
	freopen("input", "r", stdin);

	while(scanf("%d%*c%d%*c%d", &l, &w, &h) != EOF) {
		size_a = l * w;
		size_b = w * h;
		size_c = h * l;
		square_feet += min(min(size_a, size_b), size_c);
		square_feet += (2*size_a) + (2*size_b) + (2*size_c);
	}
	printf("%d\n", square_feet);
	return 0;
}
