#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#define BUFFER 10

char *md5_hash(char *);

int main()
{
	char *input = (char *) malloc(BUFFER * sizeof(char));
	freopen("input", "r", stdin);
	char *hash;
	fgets(stdin, 10, input);
	strtok(input, "\n");
	char *answer = (char *) malloc(((strlen(input) * 2) + 1) * sizeof(char));

	hash = md5_hash(answer);
	while(strstr("00000", hash) - hash != 0) {
		//change answer
		hash = md5_hash(answer);
	}

	printf("%s\n", (answer + strlen(input)));

	return 0;
}
