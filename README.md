# Advent of Code - Solutions Repository

Repository with solutions for the Advent of Code challenge.

At first there will only be `C` code, I could add more later on.

# About

Advent of Code is a series of small programming puzzles for a variety of skill levels made by Eric Wastl, also the creator of Vanilla JS, PHP Sadness, and other things.

Each puzzle will contain two parts built in the same theme, being strictly connected to Christmas.

# Organization

Each day will be represented in a separated folder with `input`, `main1.c`, `main2.c`, `puzzle.txt` files, the names are self-explanatory.
