#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void resize(int ***, int *);

#define SANTA 0
#define ROBO 1

int main()
{
	int size = 3; //initial size of matrix
	int row = 1; //initial row position
	int column = 1; //initial column position
	int houses = 1;
	int row_r = 1;
	int column_r = 1;
	freopen("input", "r", stdin);

	int **matrix = (int **) malloc(size * sizeof(int *));
	int i, j;
	for (i = 0; i < size; ++i)
		matrix[i] = (int *) calloc(size, sizeof(int));

	int turn = SANTA;
	char c;
	matrix[row][column] = 1;

	while (scanf("%c", &c) != EOF) {
		switch (c) {
			case '^':
				if (turn == SANTA) {
					if (matrix[--row][column] == 0) {
						matrix[row][column] = 1;
						houses++;
					}
					if (row == 0) {
						resize(&matrix, &size);
						row++;
						column++;
						row_r++;
						column_r++;
					}
				} else {
					if (matrix[--row_r][column_r] == 0) {
						matrix[row_r][column_r] = 1;
						houses++;
					}
					if (row_r == 0) {
						resize(&matrix, &size);
						row_r++;
						column_r++;
						row++;
						column++;
					}
				}
				break;
			case '>':
				if (turn == SANTA) {
					if (matrix[row][++column] == 0) {
						matrix[row][column] = 1;
						houses++;
					}
					if (column == size - 1) {
						resize(&matrix, &size);
						row++;
						column++;
						row_r++;
						column_r++;
					}
				} else {
					if (matrix[row_r][++column_r] == 0) {
						matrix[row_r][column_r] = 1;
						houses++;
					}
					if (column_r == size - 1) {
						resize(&matrix, &size);
						row_r++;
						column_r++;
						row++;
						column++;
					}
				}
				break;
			case 'v':
				if (turn == SANTA) {
					if (matrix[++row][column] == 0) {
						matrix[row][column] = 1;
						houses++;
					}
					if (row == size - 1) {
						resize(&matrix, &size);
						row++;
						column++;
						row_r++;
						column_r++;
					}
				} else {
					if (matrix[++row_r][column_r] == 0) {
						matrix[row_r][column_r] = 1;
						houses++;
					}
					if (row_r == size - 1) {
						resize(&matrix, &size);
						row_r++;
						column_r++;
						row++;
						column++;
					}
				}
				break;
			case '<':
				if (turn == SANTA) {
					if (matrix[row][--column] == 0) {
						matrix[row][column] = 1;
						houses++;
					}
					if (column == 0) {
						resize(&matrix, &size);
						row++;
						column++;
						row_r++;
						column_r++;
					}
				} else {
					if (matrix[row_r][--column_r] == 0) {
						matrix[row_r][column_r] = 1;
						houses++;
					}
					if (column_r == 0) {
						resize(&matrix, &size);
						row_r++;
						column_r++;
						row++;
						column++;
					}
				}
				break;
		}
		if (turn == SANTA)
			turn = ROBO;
		else
			turn = SANTA;
	}

	printf("%d\n", houses);
	for (i = 0; i < size; ++i)
		free(matrix[i]);
	free(matrix);

	return 0;
}

void resize(int ***matrix, int *size)
{
	*size += 2;
	int i, j;
	int **aux = (int **) malloc(*size * sizeof(int *));
	for (i = 0; i < *size; ++i) 
		aux[i] = (int *) calloc(*size, sizeof(int));

	for (i = 0; i < *size - 2; ++i) { 
		memcpy(&aux[i+1][1], matrix[0][i], (*size - 2) * sizeof(int)); 
		free(matrix[0][i]);
	}
	free(*matrix);
	*matrix = (int **) malloc(*size * sizeof(int *));
	for (i = 0; i < *size; ++i) {
		matrix[0][i] = (int *) malloc(*size * sizeof(int));
		memcpy(matrix[0][i], aux[i], (*size * sizeof(int)));
		free(aux[i]);
	}

	free(aux);
}
