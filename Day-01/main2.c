#include <stdio.h>

int main()
{
	freopen("input", "r", stdin);
	char c;
	int floor = 0;
	int position = 0;

	while(scanf("%c", &c) != EOF) {
		switch (c) {
			case ')':
				floor--;
				position++;
				break;
			case '(':
				floor++;
				position++;
				break;
			default:
				break;
		}
		if (floor == -1)
			break;
	}

	printf("%d\n", position);

	return 0;
}
