#include <stdio.h>

int main()
{
	freopen("input", "r", stdin);
	int floor = 0;
	char c;

	while (scanf("%c", &c) != EOF)
		switch (c) {
			case ')':
				floor--;
				break;
			case '(':
				floor++;
				break;
			default:
				break;
		}

	printf("%d\n", floor);

	return 0;
}
